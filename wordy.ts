import { filter } from './src/filter';
import { anagram } from './src/anagram';
import { contains } from './src/contains';
import { length } from './src/length';
import { max } from './src/max';
import { min } from './src/min';
import { regex } from './src/regex';
import { start as startRepl } from 'repl';
import { WordList } from './src/types/WordList';
import { WordFilter } from './src/types/WordFilter';
import { words } from './src/words';
import { score } from './src/score';

const repl = startRepl('wordy> ');

function query(...matchers: WordFilter[]): WordList {
  return filter(words, matchers);
}

Object.assign(repl.context, {
  query: query,
  anagram: anagram,
  contains: contains,
  length: length,
  max: max,
  min: min,
  regex: regex,
  score: score,
});
