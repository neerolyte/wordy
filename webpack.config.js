const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { DefinePlugin, EnvironmentPlugin } = require('webpack');

module.exports = {
  mode: 'development',
  entry: './src/index.tsx',
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/,
      },
      // word lists
      {
        test: /\.txt$/i,
        use: 'file-loader',
        exclude: /node_modules/,
      },
    ],
  },
  plugins: [
    new EnvironmentPlugin({
      CI_COMMIT_SHORT_SHA: process.env.CI_COMMIT_SHORT_SHA ?? 'development',
    }),
    new HtmlWebpackPlugin({
      title: 'Wordy',
      template: 'src/index.html',
    }),
  ],
  devtool: 'inline-source-map',
  devServer: {
    static: './public',
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js'],
  },
  output: {
    filename: '[name].[contenthash].js',
    path: path.resolve(__dirname, 'public'),
    clean: true,
  },
};
