import { WordFilter } from "./types/WordFilter";

export function max(size: number): WordFilter {
  return (word: string) => (word.length <= size);
};
