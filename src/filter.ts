import { WordFilter } from "./types/WordFilter";
import { WordList } from "./types/WordList";

export function filter(words: WordList, matchers: WordFilter[]): WordList {
  return words.filter((word) => (
    matchers.every((matcher) => (matcher(word)))
  ))
};
