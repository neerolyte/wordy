import React = require("react");

export class AbsentInput extends React.Component<{ query: string, onChange: Function}> {
  public readonly state = {
    query: '',
  }
  constructor(props: any) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event: any) {
    let value: string = event.target.value;
    this.props.onChange(value.toLowerCase());
  }

  render() {
    return (
      <label>
        Absent:
        <input type="text" value={this.props.query} onChange={this.handleChange} />
      </label>
    );
  }
}
