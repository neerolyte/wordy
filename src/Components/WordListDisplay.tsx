import React = require("react");
import { anagram } from "../anagram";
import { filter } from "../filter";
import { regex } from "../regex";
import { WordList } from "../types/WordList";
import { words } from "../words";

export class WordListDisplay extends React.Component<{anagram: string, absent: string}, { words: WordList}> {
  renderItems(words: WordList) {
    return words.slice(0,1000).map((word: string, index: number) => (
      <li key={index} id={"a" + index}>{word}</li>
    ))
  }

  render() {
    let absentRe = new RegExp(`^[^${this.props.absent}]+$`);
    return <span>
    <ul>
    {this.renderItems(filter(words, [
      anagram(this.props.anagram),
      regex(absentRe),
    ]))}
    </ul></span>
  }
}
