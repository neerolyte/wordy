import React = require("react");

export class BuildInfo extends React.Component {
  render() {
    return (
      <span>build: {process.env.CI_COMMIT_SHORT_SHA}</span>
    );
  }
}
