import { WordFilter } from "./types/WordFilter";

export function length(size: number): WordFilter {
  return (word: string) => (word.length === size);
};
