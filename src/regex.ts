import { WordFilter } from "./types/WordFilter";

/**
 * Return all words matching a supplied pattern
 */
export function regex(re: RegExp): WordFilter {
  return (word: string) => (word.match(re) !== null);
};
