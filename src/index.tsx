import React = require("react");
import { BuildInfo } from "./Components/BuildInfo";
import { WordyApp } from "./WordyApp";

const ReactDOM = require('react-dom');
ReactDOM.render(<WordyApp />, document.querySelector('#wordy_container'));
ReactDOM.render(<BuildInfo />, document.querySelector('#build_info_container'));
