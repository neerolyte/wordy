import { Word } from "./Word";

export type WordList = Word[];
