import { Score } from "./Score";
import { Word } from "./Word";

export type ScoredWord = {
  word: Word,
  score: Score,
};
