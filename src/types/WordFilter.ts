export type WordFilter = (word: string) => boolean;
