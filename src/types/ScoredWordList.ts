import { ScoredWord } from "./ScoredWord";

export type ScoredWordList = ScoredWord[];
