import { WordFilter } from "./types/WordFilter";

export function min(size: number): WordFilter {
  return (word: string) => (word.length >= size);
};
