import React = require("react");
import { AbsentInput } from "./Components/AbsentInput";
import { AnagramInput } from "./Components/AnagramInput";
import { WordListDisplay } from "./Components/WordListDisplay";
import { Queries } from "./types/Queries";

export class WordyApp extends React.Component<{}, {anagram: string, absent: string}> {
  public readonly state : Queries = {
    anagram: '?????',
    absent: '',
  }
  handleAbsentChange(absent: string) {
    this.setState({ absent: absent })
  }
  handleAnagramChange(anagram: string) {
    this.setState({ anagram: anagram })
  }
  render() {
    return [
      <AnagramInput query={this.state.anagram} onChange={(anagram: string) => {this.handleAnagramChange(anagram); }} />,
      <br/>,
      <AbsentInput query={this.state.absent} onChange={(absent: string) => {this.handleAbsentChange(absent); }} />,
      <WordListDisplay anagram={this.state.anagram} absent={this.state.absent} />,
    ];
  }
}
