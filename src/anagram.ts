import { length } from "./length";
import { WordFilter } from "./types/WordFilter";

export function anagram(letters: string): WordFilter {
  let lengthFilter = length(letters.length);
  return (word: string) => (
    lengthFilter(word)
    &&
    anagramLetterMatch(word, letters)
  );
};

function indexOrNull(haystack: string, needle: string): number|null {
  let i = haystack.indexOf(needle);
  return i >= 0 ? i : null;
}

function anagramLetterMatch(word: string, letters: string) {
  return word.split('').every((character) => {
    let i = indexOrNull(letters, character) ?? indexOrNull(letters, '?');
    if (i === null) {
      return false;
    } else {
      // remove matched letter from letters
      letters = letters.slice(0, i) + letters.slice(i+1);
    }
    return true;
  });
};
