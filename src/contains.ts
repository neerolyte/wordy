import { WordFilter } from "./types/WordFilter";

export function contains(letters: string): WordFilter {
  return (word: string) => (
    letters.split('').every((letter) => (word.includes(letter)))
  )
}
