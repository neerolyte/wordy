import { ScoredWord } from "./types/ScoredWord";
import { ScoredWordList } from "./types/ScoredWordList";
import { Word } from "./types/Word";
import { WordList } from "./types/WordList";

function scoreWord(word: Word, words: WordList): ScoredWord {
  let score = 0;
  words.forEach((listWord) => {
    score += listWord.match(new RegExp(`[${word}]`, 'g'))?.length ?? 0
  })
  return {
    word: word,
    score: score,
  }
}

export function score(words: WordList): ScoredWordList {
  return words.map((word) => (scoreWord(word, words)))
};
