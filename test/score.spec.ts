import { expect } from 'chai';
import { WordList } from '../src/types/WordList';
import { ScoredWordList } from '../src/types/ScoredWordList';
import { score } from '../src/score';

describe("score", () => {
  ([
    [['tab'], [{word:'tab',score:3}]],
    [['ball', 'lab', 'fix'], [{word:'ball',score:7},{word:'lab',score:7},{word:'fix',score:3}]],
  ] as [words: WordList, expected: ScoredWordList][]).forEach((data) => {
    let [words, expected] = data;
    it(`${words} => [${expected}]`, () => {
      expect(score(words)).to.eql(expected);
    });
  });
});
