import { expect } from 'chai';
import { anagram } from '../src/anagram';
import { WordList } from '../src/types/WordList';

describe("anagram", () => {
  let words: WordList = [
      'bar', 'bat', 'all', 'ball', 'pinball', 'tab', 'samba'
    ];
  ([
    ['abt', ['bat', 'tab']],
    ['ab', []],
    ['abll', ['ball']],
    ['bbbb', []],
    ['ab?', ['bar', 'bat', 'tab']],
    ['ab??', ['ball']],
    ['???', ['bar', 'bat', 'all', 'tab']],
    ['????', ['ball']],
  ] as [search: string, expected: WordList][]).forEach((data) => {
    let [search, expected] = data;
    it(`'${search}' => [${expected}]`, () => {
      expect(words.filter(anagram(search))).to.eql(expected);
    });
  });
});
