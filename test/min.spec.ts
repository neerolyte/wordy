import { expect } from 'chai';
import { min } from '../src/min';
import { WordList } from '../src/types/WordList';

describe('min', () => {
  let words: WordList = [
    'a', 'at', 'all', 'ball'
  ];

  ([
    [1,words],
    [2,['at', 'all', 'ball']],
    [10,[]],
    [3,['all', 'ball']],
  ] as [size: number, expected: WordList][]).forEach((data) => {
    let [size, expected] = data;
    it(`${size} => [${expected}]`, () => {
      expect(words.filter(min(size))).to.eql(expected);
    });
  });
});
