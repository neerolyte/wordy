import { expect } from 'chai';
import { regex } from '../src/regex';
import { WordList } from '../src/types/WordList';

describe('regex', () => {
  let words: WordList = [
    'bar', 'bat', 'ball', 'tab', 'samba'
  ];

  ([
    [/ba/, ['bar', 'bat', 'ball', 'samba']],
    [/bat|samba/, ['bat', 'samba']],
    [/^s/, ['samba']],
    [/a$/, ['samba']],
    [/a/, words],
    [/./, words],
  ] as [regex: RegExp, expected: WordList][]).forEach((data) => {
    let [re, expected] = data;
    it(`${re} => [${expected}]`, () => {
      expect(words.filter(regex(re))).to.eql(expected);
    });
  });
});
