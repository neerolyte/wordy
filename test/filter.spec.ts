import { expect } from 'chai';
import { filter } from '../src/filter';
import { WordList } from '../src/types/WordList';
import { WordFilter } from '../src/types/WordFilter';

describe("filter", () => {
  let words: WordList = [
      'bar', 'bat', 'all', 'ball', 'pinball', 'tab', 'samba'
    ];
  ([
    [
      '/^ba/',
      [(word: string) => (word.match(/^ba/) !== null)],
      ['bar', 'bat', 'ball']
    ],
    [
      '/^ba/ + /t$/',
      [
        (word: string) => (word.match(/^ba/) !== null),
        (word: string) => (word.match(/t$/) !== null),
      ],
      ['bat']
    ],
  ] as [description: string, matchers: WordFilter[], expected: WordList][]).forEach((data) => {
    let [description, matchers, expected] = data;
    it(`'${description}' => [${expected}]`, () => {
      expect(filter(words, matchers)).to.eql(expected);
    });
  });
});
