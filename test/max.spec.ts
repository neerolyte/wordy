import { expect } from 'chai';
import { max } from '../src/max';
import { WordList } from '../src/types/WordList';

describe('max', () => {
  let words: WordList = [
    'a', 'at', 'all', 'ball'
  ];

  ([
    [1,['a']],
    [2,['a', 'at']],
    [10,['a', 'at', 'all','ball']],
    [3,['a','at','all']],
  ] as [size: number, expected: WordList][]).forEach((data) => {
    let [size, expected] = data;
    it(`${size} => [${expected}]`, () => {
      expect(words.filter(max(size))).to.eql(expected);
    });
  });
});
