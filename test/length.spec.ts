import { expect } from 'chai';
import { length } from '../src/length';
import { WordList } from '../src/types/WordList';

describe('length', () => {
  let words: WordList = [
    'a', 'at', 'all', 'ball'
  ];

  ([
    [1,['a']],
    [2,['at']],
    [10,[]],
    [3,['all']],
  ] as [size: number, expected: WordList][]).forEach((data) => {
    let [size, expected] = data;
    it(`${size} => [${expected}]`, () => {
      expect(words.filter(length(size))).to.eql(expected);
    });
  });
});
