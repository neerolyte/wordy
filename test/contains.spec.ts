import { expect } from 'chai';
import { contains } from '../src/contains';
import { WordList } from '../src/types/WordList';

describe("contains", () => {
  const words: WordList  = ['ball', 'bar', 'bat', 'tab'];
  ([
    ['tab',['bat', 'tab']],
    ['lab',['ball']],
    ['a',['ball', 'bar', 'bat', 'tab']],
  ] as [needle: string, expected: WordList][]).forEach((data) => {
    let [needle, expected] = data;
    it(`${needle} => [${expected}]`, () => {
      expect(words.filter(contains(needle))).to.eql(expected);
    });
  });
});
