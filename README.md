# About

[![build-status](https://api.travis-ci.org/neerolyte/wordy.png)](https://travis-ci.org/neerolyte/wordy)

Wordy is here to help you find that word you're after.

Read on for examples...

# Getting started

Install dependencies:

 * install [NVM](https://github.com/nvm-sh/nvm)
 * `nvm install`
 * `npm install`

Launch a REPL shell with:

```
$ ./wordy
wordy>
```

Queries are made with `query(filters...)` e.g:


```
wordy> query(anagram('bat'))
[ 'bat', 'tab' ]
```

Queries can be combined:

```
wordy> query(anagram('a??'), contains('x'))
[
  'axe', 'fax', 'lax',
  'max', 'pax', 'rax',
  'sax', 'tax', 'wax',
  'zax'
]
```

# Queries

## anagram

The `anagram` filter will return only single word anagrams of the supplied letters in the word list, i.e. "ta" is returned for "bat".

Blank (or unknown) letters can be represented with question marks "?", e.g.:

```
wordy> query(anagram('x?'))
[ 'ax', 'ex', 'ox', 'xi', 'xu' ]
```

## contains

The `contains` filter will return only words in the word list that contain all of the provided letters:

```
wordy> query(contains('xzlch'))
[ 'chlordiazepoxide', 'chlordiazepoxides' ]
```

## length

The `length` filter will return only words of the supplied length.

E.g:

```
wordy> query(length(2))
[
  'aa', 'ab', 'ad', 'ae', 'ag', 'ah', 'ai', 'al', 'am',
  'an', 'ar', 'as', 'at', 'aw', 'ax', 'ay', 'ba', 'be',
  'bi', 'bo', 'by', 'de', 'do', 'ed', 'ef', 'eh', 'el',
  'em', 'en', 'er', 'es', 'et', 'ex', 'fa', 'go', 'ha',
  'he', 'hi', 'hm', 'ho', 'id', 'if', 'in', 'is', 'it',
  'jo', 'ka', 'la', 'li', 'lo', 'ma', 'me', 'mi', 'mm',
  'mo', 'mu', 'my', 'na', 'ne', 'no', 'nu', 'od', 'oe',
  'of', 'oh', 'om', 'on', 'op', 'or', 'os', 'ow', 'ox',
  'oy', 'pa', 'pe', 'pi', 're', 'sh', 'si', 'so', 'ta',
  'ti', 'to', 'uh', 'um', 'un', 'up', 'us', 'ut', 'we',
  'wo', 'xi', 'xu', 'ya', 'ye', 'yo'
]
```

## min

The `min` filter will return only words that are at least the supplied length.

E.g:

```
wordy> query(min(25))
[
  'electroencephalographically',
  'ethylenediaminetetraacetate',
  'ethylenediaminetetraacetates',
  'immunoelectrophoretically',
  'phosphatidylethanolamines'
]
```

## max

The `max` filter will return only words that are at most the supplied length.

E.g:

```
wordy> query(max(2))
[
  'aa', 'ab', 'ad', 'ae', 'ag', 'ah', 'ai', 'al', 'am',
  'an', 'ar', 'as', 'at', 'aw', 'ax', 'ay', 'ba', 'be',
  'bi', 'bo', 'by', 'de', 'do', 'ed', 'ef', 'eh', 'el',
  'em', 'en', 'er', 'es', 'et', 'ex', 'fa', 'go', 'ha',
  'he', 'hi', 'hm', 'ho', 'id', 'if', 'in', 'is', 'it',
  'jo', 'ka', 'la', 'li', 'lo', 'ma', 'me', 'mi', 'mm',
  'mo', 'mu', 'my', 'na', 'ne', 'no', 'nu', 'od', 'oe',
  'of', 'oh', 'om', 'on', 'op', 'or', 'os', 'ow', 'ox',
  'oy', 'pa', 'pe', 'pi', 're', 'sh', 'si', 'so', 'ta',
  'ti', 'to', 'uh', 'um', 'un', 'up', 'us', 'ut', 'we',
  'wo', 'xi', 'xu', 'ya', 'ye', 'yo'
]
```

## regex

The `regex` filter will return words in the word list matching the supplied Regular Expression:

```
wordy> query(regex(/^.x$/))
[ 'ax', 'ex', 'ox' ]
```

# Development

```
$ npm run start
```
